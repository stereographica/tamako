@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h1>zipファイル登録</h1>
      </div>
      <div class="panel-group" id="test" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div id="collapseOne" class="panel" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body center-block">
              <fieldset>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (count($messages) > 0)
                    <div class="alert alert-success">
                        <ul>
                            @foreach ($messages as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card text-center mt-5">
                  <div class="card-body ">
                    <form method="post" name="file" action="{{route('regist_zip')}}" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="File">アップロードファイル選択</label>
                        <input type="file" name="file" class="form-control-file" id="File" >
                      </div>

                      <button type="submit" class="btn btn-primary">登録</button>
                    </form>
                  </div>
                </div>

                @if ($histories->count())

                  @foreach($histories as $history)



                    <div class="card mt-5">
                      <div class="card-body ">
                          <h3 class="card-title">{{str_replace($history->zip_file_name, 'zip/', '')}} {{$statuses[$history->status]}} {{$history->updated_at}}</h3>
                      </div>
                    </div>
                  @endforeach
                @endif

              </fieldset>
            </div>
          </div>
        </div>
      </div>

    </div>
@endsection
