@extends('layouts.app')

@section('content')
    <div class="container">

      <div class="panel-group" id="test" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div id="collapseOne" class="panel" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body center-block">
              <fieldset>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                @if ($pictures->count())

                  @foreach($pictures as $picture)


                    <div class="card text-center mt-5">
                      <div class="card-body ">
                        <p>
                          <img src="{{asset($picture->file_name)}}" class="img-fluid">
                        </p>
                        <!--
                        <div class="m-3 text-left">
                          <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" id="tags-input">
                        </div>
                      -->
                        <div class="mx-auto" style="width: 310px;height:30px;">
                          <select class="rate">
                            <option></option>
                            <option value="{{$picture->id}}_1" {{$picture->rate === 1 ? 'selected=""' : ''}}>ゴミ</option>
                            <option value="{{$picture->id}}_2" {{$picture->rate === 2 ? 'selected=""' : ''}}>抜けない</option>
                            <option value="{{$picture->id}}_3" {{$picture->rate === 3 ? 'selected=""' : ''}}>萌え</option>
                            <option value="{{$picture->id}}_4" {{$picture->rate === 4 ? 'selected=""' : ''}}>抜ける</option>
                            <option value="{{$picture->id}}_5" {{$picture->rate === 5 ? 'selected=""' : ''}}>激シコ</option>
                          </select>
                        </div>
                      </div>
                    </div>

                  @endforeach

                @else
                  <H2>登録がありません</H2>
                @endif


              </fieldset>
              <nav>
                <div class="text-center">
                    @if ($pictures->count())
                        {{$pictures->links()}}
                    @endif
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>


    </div>
@endsection
