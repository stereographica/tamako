@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h1>爆シコ画像ダウンロード</h1>
      </div>
      <div class="panel-group" id="test" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div id="collapseOne" class="panel" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body center-block">
              <fieldset>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (count($messages) > 0)
                    <div class="alert alert-success">
                        <ul>
                            @foreach ($messages as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card text-center mt-5">
                  <div class="card-body ">
                    <form method="post" name="regist" action="{{route('bakushiko_regist')}}" enctype="multipart/form-data">
                      @csrf
                      <button type="submit" class="btn btn-primary">ZIPファイル生成</button>
                    </form>
                  </div>
                </div>

                @if ($histories->count())

                  @foreach($histories as $history)
                    <div class="card mt-5">
                      <div class="card-body ">
                            <h3 class="card-title">
                              {{$history->updated_at}}
                              @if ($history->status > 2)
                                <button type="button" class="btn btn-primary" onclick="window.location.href='/storage/zip/{{$history->zip_file_name}}';">Download</button>
                            </h3>
                              @else
                                 作成中
                              @endif
                      </div>
                    </div>
                  @endforeach
                @endif

              </fieldset>
            </div>
          </div>
        </div>
      </div>

    </div>
@endsection
