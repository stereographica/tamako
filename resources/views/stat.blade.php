@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h1>統計</h1>
      </div>
      <div class="panel-group" id="test" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div id="collapseOne" class="panel" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body center-block">
              <fieldset>

                <div class="card text-center mt-5">
                  <div class="card-body">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>評点</th>
                          <th>枚数</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if ($stats->count())

                          @foreach($stats as $stat)
                          <tr>
                            <td>
                              @switch($stat->rate)
                                @case(5)
                                  激シコ
                                  @break

                                @case(4)
                                  抜ける
                                  @break

                                @case(3)
                                  萌え
                                  @break

                                @case(2)
                                  抜けない
                                  @break

                                @case(1)
                                  ゴミ
                                  @break

                                @default
                                  未評価
                              @endswitch
                            </td>
                            <td>{{ $stat->count }}</td>
                          </tr>

                          @endforeach
                        @endif

                      </tbody>
                    </table>
                  </div>
                </div>



              </fieldset>
            </div>
          </div>
        </div>
      </div>

    </div>
@endsection
