<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\BulkInsert;
use App\Models\BulkInsertStatus;


class BulkInsertController extends Controller
{

    public function index()
    {
      $messages = [];

      $histories = self::get_proc_history();
      $statuses = config('my.process_status');

      return view('bulk_insert',compact('messages', 'histories', 'statuses'));
    }


    public function regist(Request $request)
    {
      logger($request);

      $this->validate($request, [
        'file' => [
          'required',
          'mimes:zip',
        ]
      ]);


      if ($request->file('file')->isValid([]))
      {
        $filename = $request->file->store('zip');

        $messages[] = 'zipをキューに追加しました。';

        $bi_models = new BulkInsertStatus;

        $bi_models->zip_file_name = $filename;
        $bi_models->status = 1;

        $bi_models->save();

        BulkInsert::dispatch($filename, $bi_models->id)
                ->delay(now()->addSeconds(10));

        $histories = self::get_proc_history();

        $statuses = config('my.process_status');

        return view('bulk_insert',compact('messages', 'histories', 'statuses'));

      }


    }

    protected function get_proc_history()
    {
      $bi_models = new BulkInsertStatus;

      $result = $bi_models->orderBy('id', 'desc')
        ->take(5)
        ->get();

      return $result;

    }
}
