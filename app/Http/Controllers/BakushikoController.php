<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\BakushikoDownload;
use App\Models\BakushikoStatus;

class BakushikoController extends Controller
{
  public function index()
  {
    $messages = [];

    $histories = self::get_proc_history();
    $statuses = config('my.process_status');

    return view('bakushiko_download',compact('messages', 'histories', 'statuses'));
  }

  public function regist()
  {
    $messages[] = 'リクエストをキューに追加しました。';

    $bs_models = new BakushikoStatus;
    $filename = md5(uniqid(rand(),1)) . '.zip';

    $bs_models->zip_file_name = $filename;
    $bs_models->status = 1;

    $bs_models->save();

    BakushikoDownload::dispatch($filename, $bs_models->id)
            ->delay(now()->addSeconds(10));

    $histories = self::get_proc_history();

    $statuses = config('my.process_status');

    return view('bakushiko_download',compact('messages', 'histories', 'statuses'));
  }

  protected function get_proc_history()
  {
    $bs_models = new BakushikoStatus;

    $result = $bs_models->orderBy('id', 'desc')
      ->take(5)
      ->get();

    return $result;

  }
}
