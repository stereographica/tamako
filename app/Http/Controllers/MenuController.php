<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Picture;
use App\Models\Rate;
use App\Models\Tag;

class MenuController extends Controller
{
    public function index()
    {
      $picture = new Picture;
      $pictures = $picture->get_pictures();

      return view('main', compact('pictures'));
    }

    public function bakushiko()
    {
      $picture = new Picture;
      $pictures = $picture->get_bakushko_pictures();

      return view('gekishiko', compact('pictures'));
    }

    public function stat()
    {
      $picture = new Picture;
      $stats = $picture->get_stat();

      return view('stat', compact('stats'));
    }
}
