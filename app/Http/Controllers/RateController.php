<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rate;

class RateController extends Controller
{
    public function add_rate(Request $request)
    {
      \Log::info('Rate API Request!!');
      \Log::info($request);

      //{picture_id}_{rate} でPOSTされる
      $buf_value = explode('_', $request->value);
      $picture_id = (int)$buf_value[0];
      $rate = (int)$buf_value[1];

      \Log::info('picture_id:'. $picture_id);
      \Log::info('rate:'. $rate);

      $rate_model = new Rate;
      return $rate_model->updateOrCreate(
        ['picture_id' => $picture_id],
        ['rate' => $rate]
      );

    }
}
