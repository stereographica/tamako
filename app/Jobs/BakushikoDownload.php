<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Picture;
use Zip;
use App\Models\BakushikoStatus;
use Illuminate\Support\Facades\Storage;

class BakushikoDownload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $zipfile, $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct($zipfile, $id)
     {
         $this->zipfile = $zipfile;
         $this->id = $id;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      \Log::info('Bakushiko Download process START!! おもちつくるよ！');
      \Log::info($this->zipfile);
      \Log::info($this->id);

      $bs_models = new BakushikoStatus;

      $bs_models->where('id', $this->id)
                ->update(['status' => 2]);

      $zip_path = 'storage/app/public/zip/' . $this->zipfile;

      $pictures = new Picture;

      $bakushiko_pictures = $pictures->get_gekishiko_zip();

      \Log::info($zip_path);


      $zip = Zip::create($zip_path);

      foreach($bakushiko_pictures as $bakushiko_picture) {
        $zip->add(str_replace('storage/img' ,'storage/app/public/img', $bakushiko_picture->file_name));
        \Log::info(str_replace('storage/img' ,'storage/app/public/img', $bakushiko_picture->file_name));
      }
      \Log::info('LIST!!!!!!!!!!!!!!!!!!!!');
      \Log::info($zip->listFiles());

      $zip->close();

      if(Zip::check($zip_path)){
        \Log::info('Bakushiko Download process END!! おもちできたよ！');
        $bs_models->where('id', $this->id)
                  ->update(['status' => 3]);
      } else {
        \Log::info('Bakushiko Download process ERROR!! おもちできなかったよ！');
      }


    }
}
