<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Picture;
use Zip;
use App\Models\BulkInsertStatus;
use Illuminate\Support\Facades\Storage;


class BulkInsert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $zipfile, $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($zipfile, $id)
    {
        $this->zipfile = $zipfile;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      \Log::info('Bulk insert process START!! おもちつくるよ！');
      \Log::info($this->zipfile);
      \Log::info($this->id);

      $bi_models = new BulkInsertStatus;

      $bi_models->where('id', $this->id)
                ->update(['status' => 2]);


      $zip_path = storage_path('app/' . $this->zipfile);

      // unzip
      $img_dir = storage_path('app/public/img');
      $unzip_dir = storage_path('app/tmp');

       if(file_exists($zip_path)){
         //$result = \Zipper::make($zip_path)->extractTo($img_dir);
         if(Zip::check($zip_path)){
           $zip = Zip::open($zip_path);
           $zip->extract($unzip_dir);
         }else {
           \Log::info('bad zip file.');
           return false;
         }


       } else {
         \Log::info('no such zip file.' . $zip_path);
         return false;
       }

       \Log::info($img_dir);
       \Log::info($unzip_dir);

       if(is_dir($unzip_dir)){
         \Log::info('is dir');
         if($dh = opendir($unzip_dir)){
           \Log::info('unzip dir open');
           while(($file = readdir($dh)) !== false){
             if(substr($file,0,1) !== '.'){
               if(filesize($unzip_dir . '/' . $file) !== false
                && filesize($unzip_dir . '/' . $file) > 0
                && @exif_imagetype($unzip_dir . '/' . $file) !== false) {
                  $file_md5 = md5_file($unzip_dir . '/' . $file);
                  $file_renamed = str_replace(substr($file,0,strrpos($file,'.')), $file_md5, $file);
                  if(file_exists($img_dir . '/' . $file_renamed)){
                    \Log::info('File Already Exists!!!!!');
                    continue;
                  }

                  if (!\File::move($unzip_dir . '/' . $file, $img_dir . '/' . $file_renamed)) {
                    \Log::info($unzip_dir . '/' . $file . 'Rename Error!!!!!!!!');
                  }
                  \Log::info($img_dir . '/' . $file_renamed);
                  $picture = new Picture;
                  $picture->file_name = 'storage/img/' . $file_renamed;
                  $picture->save();
                }

             }
           }
         } else {
           \Log::error('Failed to open directory.');
           return false;
         }
       }


       unlink($zip_path);
       \Log::info('exec(rm -rf ' . $unzip_dir . '/*)');
       exec('rm -rf ' . $unzip_dir . '/*');

       \Log::info('Bulk insert process END!! おもちできたよ！');

       $bi_models->where('id', $this->id)
                 ->update(['status' => 3]);

    }
}
