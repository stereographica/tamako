<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BulkInsertStatus extends Model
{
  protected $fillable = [
      'file_name',
      'status'
  ];


}
