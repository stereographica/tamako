<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
  protected $fillable = [
      'rate',
      'picture_id'
  ];

  public function picture()
  {
    return $this->belongsTo('App/Models/Picture');
  }
}
