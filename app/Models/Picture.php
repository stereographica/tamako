<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Picture extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name'
    ];

    public function rate()
    {
      return $this->hasOne('App\Models\Rate');
    }

    public function TagMap()
    {
      return $this->hasMany('App\Models\TagMap');
    }

    public function get_pictures()
    {
      $pictures = DB::table('pictures')
                    ->select('pictures.id', 'pictures.file_name', 'rates.rate', DB::raw('rand() as rand_val'))
                    ->leftjoin('rates', 'pictures.id', '=' , 'rates.picture_id')
                    ->orderBy('rand_val')
                    ->limit(env('PAGER_DISP_COUNT', 10));

      $pictures->where('rates.rate', '=' , NULL);

      return $pictures->get();
    }

    public function get_bakushko_pictures()
    {
        $pictures = DB::table('pictures')
            ->select('pictures.id', 'pictures.file_name', 'rates.rate')
            ->leftjoin('rates', 'pictures.id', '=' , 'rates.picture_id')
            ->where('rates.rate', '>' , 3)
            ->orderBy('pictures.created_at', 'desc')
            ->limit(env('PAGER_DISP_COUNT', 10));

        return $pictures->paginate();
    }

    public function get_gekishiko_zip()
    {
      $pictures = DB::table('pictures')
                    ->select('pictures.id', 'pictures.file_name', 'rates.rate')
                    ->leftjoin('rates', 'pictures.id', '=' , 'rates.picture_id')
                    ->orderBy('pictures.created_at', 'desc')
                    ->where('rates.rate', '>' , 3);

      return $pictures->get();
    }

    public function get_stat()
    {
      $stat = DB::table('pictures')
                ->select('rates.rate', DB::raw('count(*) as count'))
                ->leftjoin('rates', 'pictures.id', '=' , 'rates.picture_id')
                ->groupBy('rates.rate')
                ->orderBy('rates.rate', 'desc');

      return $stat->get();
    }

}
