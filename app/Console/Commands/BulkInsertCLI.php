<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Picture;
use Zip;
use Illuminate\Support\Facades\Storage;

class BulkInsertCLI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tamako:bulk {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'bulkinsert';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      \Log::info('Bulk insert process START!! おもちつくるよ！');

      $zip_path = $this->argument('file');

      // unzip
      $tmp_img_dir = '/tmp/' . md5(uniqid(rand(),1));
      $img_dir = "storage/app/public/img";

       if(file_exists($zip_path)){
         if(Zip::check($zip_path)){
           $zip = Zip::open($zip_path);
           $zip->extract($tmp_img_dir);
         }else {
           \Log::info('bad zip file.');
           return false;
         }


       } else {
         \Log::info('no such zip file.' . $zip_path);
         return false;
       }

       \Log::info($img_dir);

       if(is_dir($tmp_img_dir)){
         if($dh = opendir($tmp_img_dir)){
           while(($file = readdir($dh)) !== false){
             if(substr($file,0,1) !== '.'){
               if(filesize($tmp_img_dir . '/' . $file) !== false
                && filesize($tmp_img_dir . '/' . $file) > 0
                && @exif_imagetype($tmp_img_dir . '/' . $file) !== false) {
                  $file_md5 = md5_file($tmp_img_dir . '/' . $file);
                  $file_renamed = str_replace(substr($file,0,strrpos($file,'.')), $file_md5, $file);
                  if(file_exists($img_dir . '/' . $file_renamed)){
                    \Log::info('File Already Exists!!!!!');
                    continue;
                  }

                  if (!\File::move($tmp_img_dir . '/' . $file, $img_dir . '/' . $file_renamed)) {
                    \Log::info($tmp_img_dir . '/' . $file . 'Rename Error!!!!!!!!');
                  }

                  \Log::info($img_dir . '/' . $file_renamed);
                  $picture = new Picture;
                  $picture->file_name = 'storage/img/' . $file_renamed;
                  $picture->save();
                }

             }
           }
         } else {
           \Log::error('Failed to open directory.');
           return false;
         }
       } else {
         \Log::error('missing directory.');
         return false;
       }

       exec('rm -rf ' . $tmp_img_dir);
       exec('rm -f ' . $zip_path);

       \Log::info('Bulk insert process END!! おもちできたよ！');
    }
}
