<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\BakushikoStatus;
use App\Models\BulkInsertStatus;
use DB;

class deleteBatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tamako:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete temp files and records';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Daily delete batch start!!');
        // delete records
        $bakushiko = new BakushikoStatus();
        $bakushiko->where('created_at', '<' , DB  ::raw('(now() - interval 3 day)'))
                  ->delete();

        $bulk = new BulkInsertStatus();
        $bulk->where('created_at', '<' , DB ::raw('(now() - interval 3 day)'))
              ->delete();

        exec('rm -rf storage/app/zip/* storage/app/public/zip/*');

        \Log::info('Daily delete batch end!!');

    }
}
