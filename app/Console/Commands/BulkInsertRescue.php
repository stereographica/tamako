<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Picture;
use Zip;
use Illuminate\Support\Facades\Storage;

class BulkInsertRescue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tamako:bulkrescue {dir}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'bulkinsert';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      \Log::info('Bulk insert process START!! おもちつくるよ！');

      $dir = $this->argument('dir');
      $count = count(glob($dir));
      $bar = $this->output->createProgressBar($count);

      // unzip
      $img_dir = 'storage/app/public/img';


       \Log::info($img_dir);

       if(is_dir($dir)){
         if($dh = opendir($dir)){
           while(($file = readdir($dh)) !== false){
             if(substr($file,0,1) !== '.') {
               if(filesize($dir . '/' . $file) !== false
                && filesize($dir . '/' . $file) > 0
                && @exif_imagetype($dir . '/' . $file) !== false) {
                  \Log::info($dir . '/' . $file);
                  $file_md5 = md5_file($dir . '/' . $file);
                  $file_renamed = str_replace(substr($file,0,strrpos($file,'.')), $file_md5, $file);

                  if(file_exists($img_dir . '/' . $file_renamed)){
                    \Log::info('File Already Exists!!!!!');
                    continue;
                  }

                  if (!\File::move($dir . '/' . $file, $img_dir . '/' . $file_renamed)) {
                    \Log::info($dir . '/' . $file . 'Rename Error!!!!!!!!');
                  }
                  \Log::info($img_dir . '/' . $file_renamed);
                  $picture = new Picture;
                  $picture->file_name = 'storage/img/' . $file_renamed;
                  $picture->save();

                }

             }
             $bar->advance();
           }
         } else {
           \Log::error('Failed to open directory.');
           return false;
         }
       } else {
         \Log::error('missing directory.');
         return false;
       }

       $bar->finish();

       \Log::info('Bulk insert process END!! おもちできたよ！');
    }
}
