/**
 * jquery.keyMove.js
 *
 * @version      $Rev$
 * @author       yakumo_x (http://twitter.com/yakumo_x)
 * @copyright    (c) 2011 quoit
 * @license      The MIT License
 * @link         http://ken.quoit.jp/
 *
 * $Date$
 */

(function($){
	$.fn.keyMove=function(options){
		var c=$.extend({
			next:74,
			prev:75
		}, options);

		var t=$(this),
		offsetArr=new Array(),
		w=$(window);

		t.each(function(){
			off=$(this).offset();
			offsetArr.push(off.top);
		});
		w.keydown(function(e){
			var wt=w.scrollTop();
			if(e.keyCode==c.next){
				for (var i in offsetArr) {
					if(wt<(offsetArr[i]-1)){
						w.scrollTop(offsetArr[i]);
						break;
					}
				}
			}else if(e.keyCode==c.prev){
				for (var i in offsetArr) {
					if(wt<(offsetArr[i]+1)){
						w.scrollTop(offsetArr[i-1]);
						break;
					}
				}
			}
		});
	}
})(jQuery);
