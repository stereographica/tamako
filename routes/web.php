<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MenuController@index')->name('index');
Route::get('/bakushiko', 'MenuController@bakushiko')->name('bakushiko');
Route::get('/stat', 'MenuController@stat')->name('stat');

Route::get('/bulk', 'BulkInsertController@index')->name('bulk_insert');
Route::post('/bulk', 'BulkInsertController@regist')->name('regist_zip');

Route::get('/bakushiko_download', 'BakushikoController@index')->name('bakushiko_zip');
Route::post('/bakushiko_download', 'BakushikoController@regist')->name('bakushiko_regist');
